onstart:
    print("Workflow Start")

onsuccess:
    os.system('if [ `ls simulated_data|grep ".maf"|wc -l` -ne 0 ]; then cd simulated_data; rm *.maf *.ref;fi')
    print("Workflow finished, no error")

onerror:
    os.system('if [ `ls simulated_data|grep ".maf"|wc -l` -ne 0 ]; then cd simulated_data; rm *.maf *.ref;fi')
    print("An error has occurred.")

REF_SIM="GCF_000005845.2_ASM584v2_genomic.fna"
DEPTH_SIM="400"
MODEL="R103.model"
ERROR_SIM=[1,2,5,10,15,20,25,30]
ID_SIM=["del","ins","sub","mixte"]
SEED="123456"

THREAD="24"
READS_HIFI="SRR11434954.fastq.gz"
READS_NANOPORE_ECOLI_HIFI="SRR12801740.fastq"

READS_ILLUMINA="SRR8333590.fastq"
READS_NANOPORE_ECOLI_ILLUMINA="SRR8335315.fastq"
REF_ECOLI="GCF_004358385.1_ASM435838v1_genomic.fna"

READS_CCN="ERR4352155.fastq"
READS_BMB="ERR4352154.fastq"
READS_ILLUMINA_CCN="ERR1308732.fastq"
READS_ILLUMINA_BMB="ERR1308675.fastq"
REF_CCN="ccnscafold.fa"
REF_BMB="bmbscafold.fa"
ID=["CCN_yeast","BMB_yeast","diploid_yeast","ecoli_hifi","ecoli_illumina","human"]
CONFIGS=["_sp10","_clustal","_tcoffee","_sp100","_tcoffee_sp100"]

READS_HUMAN="reads.fastq"
REF_HUMAN="chm13.draft_v1.1.fasta"
#-------------------------------------------------------------------------------
# Start Workflow
#-------------------------------------------------------------------------------
rule all :
    input :
        expand(os.path.join('configuration_files','config{config}_simulated_{id}_{error}.yaml'), config=CONFIGS, id=ID_SIM ,error=ERROR_SIM),
        expand(os.path.join('configuration_files','config{config}_{id}.yaml'), config=CONFIGS,id=ID),
#-------------------------------------------------------------------------------
# Human data
#-------------------------------------------------------------------------------

rule download_nanopore_human_reads :
    output :
        os.path.join('human_data',READS_HUMAN)
    message :
        "Download nanopore reads for human data"
    log:
        os.path.join('logs','human_data','1_download_nanopore_reads.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'wget https://s3-us-west-2.amazonaws.com/human-pangenomics/T2T/CHM13/nanopore/rel8-guppy-5.0.7/reads.fastq.gz -o {log};'
        'gunzip ' + READS_HUMAN + '.gz >>{log} 2>&1;'
        'mv ' + READS_HUMAN + ' {output}'

rule download_human_ref :
    output :
        ref=os.path.join('human_data',REF_HUMAN),
        chrX=os.path.join('human_data',"ref_chrX_W.fasta"),
    message :
        "Download nanopore ref for human data"
    log:
        os.path.join('logs','human_data','2_download_ref.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'set +o pipefail;'
        'wget https://s3-us-west-2.amazonaws.com/human-pangenomics/T2T/CHM13/assemblies/chm13.draft_v1.1.fasta.gz -o {log};'
        'gunzip ' + REF_HUMAN + '.gz >>{log} 2>&1;'
        'mv ' + REF_HUMAN + ' {output.ref};'
        'tail -2571272 {output.ref}|head -2570994 >{output.chrX}'

rule gunzip_id_seq_chrX :
    input :
        os.path.join('id_seq_chrX.txt.gz')
    output :
        os.path.join('human_data','id_seq_chrX.txt')
    message :
        "Decompress id seq chrX for human data"
    log:
        os.path.join('logs','human_data','3_gunzip_id_seq_chrX.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'cp {input} human_data/;'
        'gunzip human_data/{input} >{log} 2>&1'
        
rule fastq2fasta_human :
    input :
        os.path.join('human_data',READS_HUMAN)
    output :
        os.path.join('human_data','reads.fasta')
    message :
        "Fastq to fasta for human reads"
    log:
        os.path.join('logs','human_data','4_fastq2fasta_human.log')
    shell:
        "sed -n '1~4s/^@/>/p;2~4p' {input} >{output}"

rule extract_fasta:
    input :
        id=os.path.join('human_data','id_seq_chrX.txt'),
        reads=os.path.join('human_data','reads.fasta')
    output:
        os.path.join('human_data','reads_chrX.fasta')
    message :
        "Extract reads for chrX"
    log:
        os.path.join('logs','human_data','5_extract_fasta.log')
    shell:
        './src/extract_reads {input.id} {input.reads} >{output}'

rule configuration_file_human_data:
    input :
        ref=os.path.join('human_data',"ref_chrX_W.fasta"),
        reads=os.path.join('human_data','reads_chrX.fasta')
    output :
        os.path.join('before_configuration_files','config_human.yaml')
    message :
        "Configuration file for human data"
    log:
        os.path.join('logs','human_data','6_configuration_file_human_data.log')
    shell:
        'PATH_DATA=`pwd`;'
        'echo -e "N: human" >{output};'
        'echo -e "I: $PATH_DATA/{input.reads}" >>{output};'
        'echo -e "R: $PATH_DATA/{input.ref}" >>{output}'
#-------------------------------------------------------------------------------
# Yeast data
#-------------------------------------------------------------------------------
rule download_CCN_yeast_reads :
    output :
        os.path.join('yeast_data',READS_CCN + '.gz')
    message :
        "Download CCN reads for yeast data"
    log:
        os.path.join('logs','yeast_data','1_download_CCN_reads.log')
    conda:
        "env_conda/fastq-dump.yaml"
    shell:
        'fastq-dump --gzip ERR4352155 >{log} 2>&1;'
        'mv ' + READS_CCN + '.gz {output}'

rule gunzip_CCN_yeast_reads :
    input :
        os.path.join('yeast_data',READS_CCN + '.gz')
    output :
        os.path.join('yeast_data',READS_CCN)
    message :
        "Decompress nanopore reads for CCN yeast"
    log:
        os.path.join('logs','yeast_data','2_gunzip_CCN_reads.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'gunzip {input} >{log} 2>&1'

rule download_BMB_yeast_reads :
    output :
        os.path.join('yeast_data',READS_BMB + '.gz')
    message :
        "Download BMB reads for yeast data"
    log:
        os.path.join('logs','yeast_data','3_download_BMB_reads.log')
    conda:
        "env_conda/fastq-dump.yaml"
    shell:
        'fastq-dump --gzip ERR4352154 >{log} 2>&1;'
        'mv ' + READS_BMB + '.gz {output}'

rule gunzip_BMB_yeast_reads :
    input :
        os.path.join('yeast_data',READS_BMB + '.gz')
    output :
        os.path.join('yeast_data',READS_BMB)
    message :
        "Decompress nanopore reads for BMB yeast"
    log:
        os.path.join('logs','yeast_data','4_gunzip_BMB_reads.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'gunzip {input} >{log} 2>&1'


rule download_CCN_yeast_reads_illumina :
    output :
        os.path.join('yeast_data',READS_ILLUMINA_CCN + '.gz')
    message :
        "Download CCN reads illumina for yeast data"
    log:
        os.path.join('logs','yeast_data','5_download_CCN_reads_illumina.log')
    conda:
        "env_conda/fastq-dump.yaml"
    shell:
        'fastq-dump --gzip --split-spot  ERR1308732 >{log} 2>&1;'
        'mv ' + READS_ILLUMINA_CCN + '.gz {output}'

rule gunzip_CCN_yeast_reads_illumina :
    input :
        os.path.join('yeast_data',READS_ILLUMINA_CCN + '.gz')
    output :
        os.path.join('yeast_data',READS_ILLUMINA_CCN)
    message :
        "Decompress illumina reads for CCN yeast"
    log:
        os.path.join('logs','yeast_data','6_gunzip_CCN_reads_illumina.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'gunzip {input} >{log} 2>&1'

rule download_BMB_yeast_reads_illumina :
    output :
        os.path.join('yeast_data',READS_ILLUMINA_BMB + '.gz')
    message :
        "Download BMB reads illumina for yeast data"
    log:
        os.path.join('logs','yeast_data','7_download_BMB_reads_illumina.log')
    conda:
        "env_conda/fastq-dump.yaml"
    shell:
        'fastq-dump --gzip --split-spot  ERR1308675 >{log} 2>&1;'
        'mv ' + READS_ILLUMINA_BMB + '.gz {output}'

rule gunzip_BMB_yeast_reads_illumina :
    input :
        os.path.join('yeast_data',READS_ILLUMINA_BMB + '.gz')
    output :
        os.path.join('yeast_data',READS_ILLUMINA_BMB)
    message :
        "Decompress illumina reads for BMB yeast"
    log:
        os.path.join('logs','yeast_data','8_gunzip_BMB_reads_illumina.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'gunzip {input} >{log} 2>&1'

rule assembly_illumina_reads_CCN :
    input :
            illumina=os.path.join('yeast_data',READS_ILLUMINA_CCN + '.gz'),
            nanopore=os.path.join('yeast_data',READS_CCN)
    output:
            os.path.join('yeast_data','assembly_illumina_ccn','contigs.fasta')
    message :
            "Assembly illumina reads for CCN yeast data with spades"
    log:
            os.path.join('logs','yeast_data','9_assembly_illumina_reads_ccn.log')
    conda:
            "env_conda/spades.yaml"
    shell:
            'spades.py --12 {input.illumina} --nanopore {input.nanopore} -o yeast_data/assembly_illumina_ccn --only-assembler -t '+THREAD+' >{log} 2>&1'

rule assembly_illumina_reads_BMB :
    input :
            illumina=os.path.join('yeast_data',READS_ILLUMINA_BMB + '.gz'),
            nanopore=os.path.join('yeast_data',READS_BMB)
    output:
            os.path.join('yeast_data','assembly_illumina_bmb','contigs.fasta')
    message :
            "Assembly illumina reads for BMB yeast data with spades"
    log:
            os.path.join('logs','yeast_data','10_assembly_illumina_reads_BMB.log')
    conda:
            "env_conda/spades.yaml"
    shell:
            'spades.py --12 {input.illumina} --nanopore {input.nanopore} -o yeast_data/assembly_illumina_bmb --only-assembler -t '+THREAD+' >{log} 2>&1'

rule reduced_ref_bmb:
    input :
        os.path.join('yeast_data','assembly_illumina_bmb','contigs.fasta')
    output :
        os.path.join('yeast_data',"reduced_assembly_bmb.fasta")
    message :
        "Reduced reference for bmb yeast"
    log:
        os.path.join('logs','yeast_data','11_reduced_bmb_ref.log')
    shell:
        'set +o pipefail;'
        'LINE=`cat {input} |grep -n ">"|head -2| cut -d: -f1|tail -1`;'
        'head -$(($LINE-1)) {input} >{output} 2>>{log}'

rule reduced_ref_ccn:
    input :
        os.path.join('yeast_data','assembly_illumina_ccn','contigs.fasta')
    output :
        os.path.join('yeast_data',"reduced_assembly_ccn.fasta")
    message :
        "Reduced reference for ccn yeast"
    log:
        os.path.join('logs','yeast_data','12_reduced_ccn_ref.log')
    shell:
        'set +o pipefail;'
        'LINE_6=`cat {input} |grep -n ">"|head -6| cut -d: -f1|tail -1`;'
        'LINE_5=`cat {input} |grep -n ">"|head -5| cut -d: -f1|tail -1`;'
        'head -$(($LINE_6 - 1)) {input} | tail -$(($LINE_6 - $LINE_5)) >{output} 2>>{log}'

rule align_ref_BMB_ref_CCN :
    input :
        ccn=os.path.join('yeast_data',"reduced_assembly_ccn.fasta"),
        bmb=os.path.join('yeast_data',"reduced_assembly_bmb.fasta")
    output:
        os.path.join('yeast_data','aln_reduced_assembly_bmb_ccn.sam')
    message :
        "Align BMB ref with CCN ref with minimap"
    log:
        os.path.join('logs','yeast_data','13_align_ref_bmb_ref_ccn.log')
    conda:
        "env_conda/minimap2.yaml"
    shell:
        'minimap2 -cax asm5  -t '+THREAD+' {input.bmb} {input.ccn} >{output} 2>{log}'

rule reads_map_region_yeast :
    input :
        os.path.join('yeast_data','aln_reduced_assembly_bmb_ccn.sam')
    output :
        os.path.join('yeast_data','ref_ccn.fasta')
    message:
        "Reads map region for yeast data"
    log:
        os.path.join('logs','yeast_data','14_reads_map_region.log')
    conda:
        "env_conda/perl.yaml"
    shell :
        'DEB=1356150;END=1791934;'
        './src/reads_map_region.pl -s $DEB -e $END {input} {output} >{log} 2>&1'

rule region_seq_yeast :
    input :
        os.path.join('yeast_data',"reduced_assembly_bmb.fasta")
    output :
        start=os.path.join('yeast_data','start_region.txt'),
        ref=os.path.join('yeast_data','ref_bmb.fasta')
    message:
        "Region seq for yeast data"
    log:
        os.path.join('logs','yeast_data','15_region_seq_yeast.log')
    shell :
        'DEB=1356150;END=1791934;'
        'echo $DEB >{output.start};'
        'SIZE=$(($END-$DEB));'
        './src/region_seq.sh {output.start} {input} {output.ref} $SIZE >{log} 2>&1'

rule configuration_file_CCN_yeast_data:
    input :
        ref=os.path.join('yeast_data','ref_ccn.fasta'),
        reads=os.path.join('yeast_data',READS_CCN)
    output :
        os.path.join('before_configuration_files','config_CCN_yeast.yaml')
    message :
        "Configuration file for CCN yeast data"
    log:
        os.path.join('logs','yeast_data','16_configuration_file_CCN_yeast_data.log')
    shell:
        'PATH_DATA=`pwd`;'
        'echo -e "N: CCN_yeast" >{output};'
        'echo -e "I: $PATH_DATA/{input.reads}" >>{output};'
        'echo -e "R: $PATH_DATA/{input.ref}" >>{output}'

rule configuration_file_BMB_yeast_data:
    input :
        ref=os.path.join('yeast_data','ref_bmb.fasta'),
        reads=os.path.join('yeast_data',READS_BMB)
    output :
        os.path.join('before_configuration_files','config_BMB_yeast.yaml')
    message :
        "Configuration file for BMB yeast data"
    log:
        os.path.join('logs','yeast_data','17_configuration_file_BMB_yeast_data.log')
    shell:
        'PATH_DATA=`pwd`;'
        'echo -e "N: BMB_yeast" >{output};'
        'echo -e "I: $PATH_DATA/{input.reads}" >>{output};'
        'echo -e "R: $PATH_DATA/{input.ref}" >>{output}'

rule diploid_reads:
    input :
        ccn=os.path.join('yeast_data',READS_CCN),
        bmb=os.path.join('yeast_data',READS_BMB)
    output :
        os.path.join('yeast_data','diploid_reads.fastq')
    message :
        "Creation of diploid yeast reads"
    log:
        os.path.join('logs','yeast_data','18_diploid_reads.log')
    shell:
        'set +o pipefail;'
        'cat {input.bmb} |head -260000 >{output};'
        'cat {input.ccn} |head -260000 >>{output}'

rule diploid_reads_shuffle:
    input :
        os.path.join('yeast_data','diploid_reads.fastq')
    output :
        os.path.join('yeast_data','diploid_reads_shuffle.fastq')
    message :
        "Shuffle diploid reads for yeast data"
    log:
        os.path.join('logs','yeast_data','19_diploid_reads_shuffle.log')
    shell:
        './src/shuffle_fastq.pl {input} >{output} 2>{log}'

rule align_ref_BMB_ref_CCN_exonerate :
    input :
        ccn=os.path.join('yeast_data',"ref_ccn.fasta"),
        bmb=os.path.join('yeast_data',"ref_bmb.fasta")
    output:
        os.path.join('yeast_data','aln_refs_bmb_ccn.txt')
    message :
        "Align BMB ref with CCN ref with exonerate"
    log:
        os.path.join('logs','yeast_data','20_align_ref_BMB_ref_CCN_exonerate.log')
    conda:
        "env_conda/exonerate.yaml"
    shell:
        'exonerate -m a:g --bigseq yes -Q dna -E --bestn 1 --dnahspthreshold 120 {input.bmb} {input.ccn} >{output} 2>{log}'

rule exonerate_to_fasta :
    input :
        os.path.join('yeast_data','aln_refs_bmb_ccn.txt')
    output:
        os.path.join('yeast_data','aln_refs_bmb_ccn.fasta')
    message :
        "Exonerate to fasta"
    log:
        os.path.join('logs','yeast_data','20_bis_exonerate_to_fasta.log')
    shell:
       './src/exonerate_to_fasta.pl <{input} >{output} 2>{log}'

rule ref_diploide :
    input :
        os.path.join('yeast_data','aln_refs_bmb_ccn.fasta')
    output:
        os.path.join('yeast_data','ref_diploid.fasta')
    message :
        "Creation of diploid yeast ref"
    log:
        os.path.join('logs','yeast_data','21_ref_diploid.log')
    conda:
        "env_conda/python3.yaml"
    shell:
        './src/sequence_consensus.py -I -in {input} -t 100 -o {output} 2>{log}'

rule configuration_file_diploid_yeast_data:
    input :
        ref=os.path.join('yeast_data','ref_diploid.fasta'),
        reads=os.path.join('yeast_data','diploid_reads_shuffle.fastq')
    output :
        os.path.join('before_configuration_files','config_diploid_yeast.yaml')
    message :
        "Configuration file for diploid yeast data"
    log:
        os.path.join('logs','yeast_data','22_configuration_file_diploid_yeast_data.log')
    shell:
        'PATH_DATA=`pwd`;'
        'echo -e "N: diploid_yeast" >{output};'
        'echo -e "I: $PATH_DATA/{input.reads}" >>{output};'
        'echo -e "R: $PATH_DATA/{input.ref}" >>{output}'

#-------------------------------------------------------------------------------
# Ecoli hifi data
#-------------------------------------------------------------------------------
rule download_hifi_reads_ecoli :
    output :
        os.path.join('ecoli_hifi_data',READS_HIFI)
    message :
        "Download hifi reads for ecoli hifi data"
    log:
        os.path.join('logs','ecoli_hifi_data','1_download_hifi.log')
    conda:
        "env_conda/fastq-dump.yaml"
    shell:
        'fastq-dump --gzip SRR11434954 >{log} 2>&1;'
        'mv ' + READS_HIFI + ' {output}'

rule download_nanopore_reads_ecoli_hifi :
    output :
        os.path.join('ecoli_hifi_data',READS_NANOPORE_ECOLI_HIFI + '.gz')
    message :
        "Download nanopore reads for ecoli hifi data"
    log:
        os.path.join('logs','ecoli_hifi_data','2_download_nanopore.log')
    conda:
        "env_conda/fastq-dump.yaml"
    shell:
        'fastq-dump --gzip SRR12801740 >{log} 2>&1;'
        'mv ' + READS_NANOPORE_ECOLI_HIFI + '.gz {output}'

rule gunzip_nanopore_reads_ecoli_hifi :
    input :
        os.path.join('ecoli_hifi_data',READS_NANOPORE_ECOLI_HIFI + '.gz')
    output :
        os.path.join('ecoli_hifi_data',READS_NANOPORE_ECOLI_HIFI)
    message :
        "Decompress nanopore reads for ecoli hifi data"
    log:
        os.path.join('logs','ecoli_hifi_data','3_gunzip_nanopore_reads.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'gunzip {input} >{log} 2>&1'

rule select_hifi_reads_ecoli :
    input :
        os.path.join('ecoli_hifi_data',READS_HIFI)
    output:
        os.path.join('ecoli_hifi_data','reads_hifi.fastq')
    message :
        "Select hifi reads for ecoli hifi data"
    log:
        os.path.join('logs','ecoli_hifi_data','4_select_hifi_reads.log')
    conda:
        "env_conda/seqkit.yaml"
    shell:
        'set +o pipefail;'
        'seqkit sort -l -r {input} 2>{log}|head -100000 >{output} 2>{log}'

rule assembly_hifi_reads_ecoli :
    input :
        os.path.join('ecoli_hifi_data','reads_hifi.fastq')
    output:
        os.path.join('ecoli_hifi_data','assembly_hifi','assembly.fasta')
    message :
        "Assembly hifi reads for ecoli hifi data with flye"
    log:
        os.path.join('logs','ecoli_hifi_data','5_assembly_hifi_reads.log')
    conda:
        "env_conda/flye.yaml"
    shell:
        'flye --pacbio-hifi {input} --genome-size 5m --threads ' + THREAD + ' -i 2 -o ecoli_hifi_data/assembly_hifi >{log} 2>&1'

rule extract_first_contig_hifi_reads :
    input :
        os.path.join('ecoli_hifi_data','assembly_hifi','assembly.fasta')
    output:
        os.path.join('ecoli_hifi_data','ref_ecoli_hifi.fasta')
    message :
        "Extract first contig assembly for ecoli hifi data"
    log:
        os.path.join('logs','ecoli_hifi_data','6_extract_first_contig_hifi_reads.log')
    shell:
        'set +o pipefail;'
        'cat {input}|tr "\\n" "@"|sed "s/@>/\\n>/g"|sed "s/@/\\n/;s/@//g"|head -2 >{output} 2>{log}'

rule configuration_file_ecoli_hifi_data:
    input :
        ref=os.path.join('ecoli_hifi_data','ref_ecoli_hifi.fasta'),
        reads=os.path.join('ecoli_hifi_data',READS_NANOPORE_ECOLI_HIFI)
    output :
        ref=os.path.join('before_configuration_files','config_ecoli_hifi.yaml')
    message :
        "Configuration file for ecoli hifi"
    log:
        os.path.join('logs','ecoli_hifi_data','7_configuration_ecoli_hifi.log')
    shell:
        'PATH_DATA=`pwd`;'
        'echo -e "N: ecoli_hifi" >{output};'
        'echo -e "I: $PATH_DATA/{input.reads}" >>{output};'
        'echo -e "R: $PATH_DATA/{input.ref}" >>{output}'

#-------------------------------------------------------------------------------
# Ecoli illumina data
#-------------------------------------------------------------------------------
rule download_illumina_reads_ecoli :
    output :
        os.path.join('ecoli_illumina_data',READS_ILLUMINA+ '.gz')
    message :
        "Download illumina reads for ecoli illumina data"
    log:
        os.path.join('logs','ecoli_illumina_data','1_download_illumina.log')
    conda:
        "env_conda/fastq-dump.yaml"
    shell:
        'fastq-dump --gzip --split-spot --clip SRR8333590 >{log} 2>&1;'
        'mv ' + READS_ILLUMINA + '.gz {output}'

rule gunzip_illumina_reads_ecoli_illumina:
    input :
        os.path.join('ecoli_illumina_data',READS_ILLUMINA + '.gz')
    output :
        os.path.join('ecoli_illumina_data',READS_ILLUMINA)
    message :
        "Decompress illumina reads for ecoli illumina data"
    log:
        os.path.join('logs','ecoli_illumina_data','2_gunzip_illumina_reads.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'gunzip {input} >{log} 2>&1'

rule download_nanopore_reads_ecoli_illumina :
    output :
        os.path.join('ecoli_illumina_data',READS_NANOPORE_ECOLI_ILLUMINA + '.gz')
    message :
        "Download nanopore reads for ecoli illumina data"
    log:
        os.path.join('logs','ecoli_illumina_data','3_download_nanopore.log')
    conda:
        "env_conda/fastq-dump.yaml"
    shell:
        'fastq-dump --gzip SRR8335315 >{log} 2>&1;'
        'mv ' + READS_NANOPORE_ECOLI_ILLUMINA + '.gz {output}'

rule gunzip_nanopore_reads_ecoli_illumina :
    input :
        os.path.join('ecoli_illumina_data',READS_NANOPORE_ECOLI_ILLUMINA + '.gz')
    output :
        os.path.join('ecoli_illumina_data',READS_NANOPORE_ECOLI_ILLUMINA)
    message :
        "Decompress nanopore reads for ecoli illumina data"
    log:
        os.path.join('logs','ecoli_illumina_data','4_gunzip_nanopore_reads.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'gunzip {input} >{log} 2>&1'

rule download_ref_ecoli :
    output :
        os.path.join('ecoli_illumina_data',REF_ECOLI + '.gz')
    message :
        "Download ref ecoli for ecoli illumina data"
    log:
        os.path.join('logs','ecoli_illumina_data','5_download_ref_ecoli.log')
    shell:
        'wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/004/358/385/GCF_004358385.1_ASM435838v1/GCF_004358385.1_ASM435838v1_genomic.fna.gz -o {log};'
        'mv ' + REF_ECOLI + '.gz {output}'

rule gunzip_ref_ecoli :
    input :
        os.path.join('ecoli_illumina_data',REF_ECOLI+ '.gz')
    output :
        os.path.join('ecoli_illumina_data',REF_ECOLI)
    message :
        "Decompress ref ecoli for ecoli illumina data"
    log:
        os.path.join('logs','ecoli_illumina_data','6_gunzip_ref_ecoli.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'gunzip {input} >{log} 2>&1'

rule assembly_illumina_reads_ecoli :
    input :
        os.path.join('ecoli_illumina_data',READS_ILLUMINA)
    output:
        os.path.join('ecoli_illumina_data','assembly_illumina','contigs.fasta')
    message :
        "Assembly illumina reads for ecoli illumina data with spades"
    log:
        os.path.join('logs','ecoli_illumina_data','7_assembly_illumina_reads.log')
    conda:
        "env_conda/spades.yaml"
    shell:
        'spades.py -s {input} -o ecoli_illumina_data/assembly_illumina --only-assembler -t '+THREAD+' >{log} 2>&1'

rule extract_first_chromosome_ref :
    input :
        os.path.join('ecoli_illumina_data',REF_ECOLI)
    output:
        os.path.join('ecoli_illumina_data','ref_ecoli.fasta')
    message :
        "Extract first chromosome ref for ecoli illumina data"
    log:
        os.path.join('logs','ecoli_illumina_data','8_extract_first_chromosome_ref.log')
    shell:
        'head -67952 {input} >{output} 2>{log}'

rule align_illumina_contig_ref :
    input :
        contigs=os.path.join('ecoli_illumina_data','assembly_illumina','contigs.fasta'),
        ref=os.path.join('ecoli_illumina_data','ref_ecoli.fasta')
    output:
        os.path.join('ecoli_illumina_data','aln_contigs_illumina_ref_ecoli.sam')
    message :
        "Align illumina contigs with ref for ecoli illumina data with minimap"
    log:
        os.path.join('logs','ecoli_illumina_data','9_align_illumina_contig_ref.log')
    conda:
        "env_conda/minimap2.yaml"
    shell:
        'minimap2 -cax asm5  -t '+THREAD+' {input.ref} {input.contigs} >{output} 2>{log}'

rule reads_map_region_ecoli_illumina :
    input :
        os.path.join('ecoli_illumina_data','aln_contigs_illumina_ref_ecoli.sam'),
    output :
        os.path.join('ecoli_illumina_data','ref_illumina_ecoli.fasta')
    message:
        "Reads map region for ecoli illumina data"
    log:
        os.path.join('logs','ecoli_illumina_data','10_reads_map_region.log')
    conda:
        "env_conda/perl.yaml"
    shell :
        'DEB=4845200;END=5080000;'
        './src/reads_map_region.pl -s $DEB -e $END {input} {output} >{log} 2>&1'

rule configuration_file_ecoli_illumina_data:
    input :
        ref=os.path.join('ecoli_illumina_data','ref_illumina_ecoli.fasta'),
        reads=os.path.join('ecoli_illumina_data',READS_NANOPORE_ECOLI_ILLUMINA)
    output :
        ref=os.path.join('before_configuration_files','config_ecoli_illumina.yaml')
    message :
        "Configuration file for ecoli illumina"
    log:
        os.path.join('logs','ecoli_illumina_data','11_configuration_ecoli_illumina.log')
    shell:
        'PATH_DATA=`pwd`;'
        'echo -e "N: ecoli_illumina" >{output};'
        'echo -e "I: $PATH_DATA/{input.reads}" >>{output};'
        'echo -e "R: $PATH_DATA/{input.ref}" >>{output}'

#-------------------------------------------------------------------------------
# Simulated data
#-------------------------------------------------------------------------------
rule download_ref_simulated_data :
    output :
        os.path.join('simulated_data',REF_SIM + '.gz')
    message :
        "Download reference for simulated data"
    log:
        os.path.join('logs','simulated_data','1_download_ref.log')
    shell:
        'wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz -o {log} ;'
        'mv ' + REF_SIM + '.gz {output}'

rule gunzip_ref_simulated_data :
    input :
        os.path.join('simulated_data',REF_SIM + '.gz')
    output :
        os.path.join('simulated_data',REF_SIM)
    message :
        "Decompress reference for simulated data"
    log:
        os.path.join('logs','simulated_data','2_gunzip_ref.log')
    conda:
        "env_conda/gzip.yaml"
    shell:
        'gunzip {input} >{log} 2>&1'

rule download_model_simulated_data :
    output :
        os.path.join('simulated_data',MODEL)
    message :
        "Download model for simulated data"
    log:
        os.path.join('logs','simulated_data','3_download_model.log')
    shell:
        'wget https://github.com/yukiteruono/pbsim2/raw/master/data/R103.model -o {log};'
        'mv ' + MODEL + ' {output}'

rule simulated_data_del :
    input :
        ref=os.path.join('simulated_data',REF_SIM),
        model=os.path.join('simulated_data',MODEL)
    output :
        os.path.join('simulated_data','simulated_del_{error_sim}_0001.fastq')
    message :
        "Generation of simulated data with only deletion (error={wildcards.error_sim})"
    log:
        os.path.join('logs','simulated_data','4_simulated_data_del_{error_sim}.log')
    conda:
        "env_conda/pbsim2.yaml"
    shell:
        'ACCURACY=$((100-{wildcards.error_sim}));'
        'PREFIX=`echo {output}|sed "s/_0001.fastq//"`;'
        'RATIO=00:00:100;'
        'DEPTH='+ DEPTH_SIM +';'
        'SEED='+ SEED +';'
        'pbsim {input.ref} --seed $SEED --hmm_model {input.model} --depth $DEPTH --difference-ratio $RATIO --accuracy-mean 0.$ACCURACY --prefix $PREFIX >{log} 2>&1'

rule simulated_data_ins :
    input :
        ref=os.path.join('simulated_data',REF_SIM),
        model=os.path.join('simulated_data',MODEL)
    output :
        os.path.join('simulated_data','simulated_ins_{error_sim}_0001.fastq')
    message :
        "Generation of simulated data with only insertion (error={wildcards.error_sim})"
    log:
        os.path.join('logs','simulated_data','5_simulated_data_ins_{error_sim}.log')
    conda:
        "env_conda/pbsim2.yaml"
    shell:
        'ACCURACY=$((100-{wildcards.error_sim}));'
        'PREFIX=`echo {output}|sed "s/_0001.fastq//"`;'
        'RATIO=00:100:00;'
        'DEPTH='+ DEPTH_SIM +';'
        'SEED='+ SEED +';'
        'pbsim {input.ref} --seed $SEED --hmm_model {input.model} --depth $DEPTH --difference-ratio $RATIO --accuracy-mean 0.$ACCURACY --prefix $PREFIX >{log} 2>&1'

rule simulated_data_sub :
    input :
        ref=os.path.join('simulated_data',REF_SIM),
        model=os.path.join('simulated_data',MODEL)
    output :
        os.path.join('simulated_data','simulated_sub_{error_sim}_0001.fastq')
    message :
        "Generation of simulated data with only substitution (error={wildcards.error_sim})"
    log:
        os.path.join('logs','simulated_data','6_simulated_data_sub_{error_sim}.log')
    conda:
        "env_conda/pbsim2.yaml"
    shell:
        'ACCURACY=$((100-{wildcards.error_sim}));'
        'PREFIX=`echo {output}|sed "s/_0001.fastq//"`;'
        'RATIO=100:00:00;'
        'DEPTH='+ DEPTH_SIM +';'
        'SEED='+ SEED +';'
        'pbsim {input.ref} --seed $SEED --hmm_model {input.model} --depth $DEPTH --difference-ratio $RATIO --accuracy-mean 0.$ACCURACY --prefix $PREFIX >{log} 2>&1'

rule simulated_data_mixte :
    input :
        ref=os.path.join('simulated_data',REF_SIM),
        model=os.path.join('simulated_data',MODEL)
    output :
        os.path.join('simulated_data','simulated_mixte_{error_sim}_0001.fastq')
    message :
        "Generation of simulated data with 23 substitution, 31 insertion and 46 deletion (error={wildcards.error_sim})"
    log:
        os.path.join('logs','simulated_data','7_simulated_data_mixte_{error_sim}.log')
    conda:
        "env_conda/pbsim2.yaml"
    shell:
        'ACCURACY=$((100-{wildcards.error_sim}));'
        'PREFIX=`echo {output}|sed "s/_0001.fastq//"`;'
        'RATIO=23:31:46;'
        'DEPTH='+ DEPTH_SIM +';'
        'SEED='+ SEED +';'
        'pbsim {input.ref} --seed $SEED --hmm_model {input.model} --depth $DEPTH --difference-ratio $RATIO --accuracy-mean 0.$ACCURACY --prefix $PREFIX >{log} 2>&1'

rule configuration_file_simulated_data:
    input :
        ref=os.path.join('simulated_data',REF_SIM),
        reads=os.path.join('simulated_data','simulated_{id_sim}_{error_sim}_0001.fastq')
    output :
        ref=os.path.join('before_configuration_files','config_simulated_{id_sim}_{error_sim}.yaml')
    message :
        "Configuration file for simulated data ({wildcards.id_sim}_{wildcards.error_sim})"
    log:
        os.path.join('logs','simulated_data','8_configuration_file_simulated_{id_sim}_{error_sim}.log')
    shell:
        'PATH_DATA=`pwd`;'
        'NAME_SIMULATED=`echo {input.reads}|cut -d"/" -f 2|sed "s/_0001.fastq//"`;'
        'echo -e "N: $NAME_SIMULATED" >{output};'
        'echo -e "I: $PATH_DATA/{input.reads}" >>{output};'
        'echo -e "R: $PATH_DATA/{input.ref}" >>{output}'

#-------------------------------------------------------------------------------
# Configuration files
#-------------------------------------------------------------------------------
rule configuration_files_sp10:
    input :
        os.path.join('before_configuration_files','config_{id}.yaml')
    output :
        os.path.join('configuration_files','config_sp10_{id}.yaml')
    message :
        "Configuration files for sp10 ({wildcards.id})"
    log:
        os.path.join('logs','configuration_files','1_configuration_files_for_sp10_{id}.log')
    shell:
        'set +o pipefail;'
        'head -1 {input}| sed "s/$/_sp10/" >{output};'
        'tail -2 {input} >>{output};'
        'echo -e "D: [10,20,30,45,50,60,100,150,200]" >>{output};'
        'echo -e "S: [100,200,500,1000,2000,5000,10000]" >>{output};'
        'echo -e "T: [30,50,60,70,80]" >>{output};'
        'echo -e "M: [muscle,mafft,poa,spoa,abpoa,kalign,kalign3]" >>{output};'
        'echo -e "O: 10" >>{output}'
        

rule configuration_files_for_clustal:
    input :
        os.path.join('before_configuration_files','config_{id}.yaml')
    output :
        os.path.join('configuration_files','config_clustal_{id}.yaml')
    message :
        "Configuration files for clustal ({wildcards.id})"
    log:
        os.path.join('logs','configuration_files','2_configuration_files_for_clustal_{id}.log')
    shell:
        'set +o pipefail;'
        'head -1 {input}| sed "s/$/_clustal/" >{output};'
        'tail -2 {input} >>{output};'
        'echo -e "D: [10,20,30,45,50,60,100,150,200]" >>{output};'
        'echo -e "S: [100,200,500,1000,2000,5000,10000]" >>{output};'
        'echo -e "T: [30,50,60,70,80]" >>{output};'
        'echo -e "M: [clustalo]" >>{output};'
        'echo -e "O: 10" >>{output}'

rule configuration_files_for_tcoffee:
    input :
        config=os.path.join('before_configuration_files','config_{id}.yaml'),
        start=os.path.join('start','start_for_{id}.txt')
    output :
        os.path.join('configuration_files','config_tcoffee_{id}.yaml')
    message :
        "Configuration files for tcoffee ({wildcards.id})"
    log:
        os.path.join('logs','configuration_files','3_configuration_files_for_tcoffee_{id}.log')
    shell:
        'set +o pipefail;'
        'head -1 {input.config}| sed "s/$/_tcoffee/" >{output};'
        'tail -2 {input.config} >>{output};'
        'echo -e "D: [10,20,30,45,50,60,100]" >>{output};'
        'echo -e "S: [100,200,500,1000]" >>{output};'
        'echo -e "T: [30,50,60,70,80]" >>{output};'
        'echo -e "M: [tcoffee]" >>{output};'
        'echo -e "B: [`cat {input.start}`]" >>{output}'
        
rule configuration_files_for_sp100:
    input :
        os.path.join('before_configuration_files','config_{id}.yaml'),
    output :
        os.path.join('configuration_files','config_sp100_{id}.yaml')
    message :
        "Configuration files for sp100 ({wildcards.id})"
    log:
        os.path.join('logs','configuration_files','3_configuration_files_for_sp100_{id}.log')
    shell:
        'set +o pipefail;'
        'head -1 {input}| sed "s/$/_sp100/" >{output};'
        'tail -2 {input} >>{output};'
        'echo -e "D: [10,20,30,45,50,60,100,150,200]" >>{output};'
        'echo -e "S: [500]" >>{output};'
        'echo -e "T: [30,50,60,70,80]" >>{output};'
        'echo -e "M: [muscle,mafft,poa,spoa,abpoa,kalign,kalign3]" >>{output};'
        'echo -e "O: 100" >>{output}'

rule configuration_files_for_sp100_tcoffee:
    input :
        os.path.join('before_configuration_files','config_{id}.yaml'),
    output :
        os.path.join('configuration_files','config_tcoffee_sp100_{id}.yaml')
    message :
        "Configuration files for sp100 tcoffee ({wildcards.id})"
    log:
        os.path.join('logs','configuration_files','3_configuration_files_for_tcoffee_sp100_{id}.log')
    shell:
        'set +o pipefail;'
        'head -1 {input}| sed "s/$/_sp100_tcoffee/" >{output};'
        'tail -2 {input} >>{output};'
        'echo -e "D: [10,20,30,45,50,60,100,150,200]" >>{output};'
        'echo -e "S: [500]" >>{output};'
        'echo -e "T: [30,50,60,70,80]" >>{output};'
        'echo -e "M: [tcoffee]" >>{output};'
        'echo -e "O: 100" >>{output}'
