#seqkit sort -r -l -2 -w 0 reads.fasta >reads_sort_2.fasta
cd human_data
nb_reads=`cat reads_sort_2.fasta | grep -c ">"`
echo "nb_reads=$nb_reads"
cat reads_sort_2.fasta| grep -v INFO >reads_sort.fasta
percentages="10 20 30 40"
for p in $percentages; do echo $p;head -$(($nb_reads*p/100*2)) reads_sort.fasta >reads_$p.fasta;done
