#!/bin/bash
THREAD="24"

if [ ! -d "ecoli_hifi_data" ];then
mkdir "ecoli_hifi_data"
fi

cd ecoli_hifi_data

echo "Download hifi reads..."
#fastq-dump --gzip SRR11434954

echo "Download nanopore reads..."
#fastq-dump --gzip SRR12801740
#gunzip SRR12801740.fastq.gz >/dev/null

echo "Select hifi reads..."
#seqkit sort -l -r SRR11434954.fastq.gz|head -100000 >read_hifi.fastq

echo "Assembly of hifi (flye)"
#flye --pacbio-hifi read_hifi.fastq --genome-size 5m --threads $THREAD -i 2 -o ass_hifi

echo "Extract first contig"
#cat ass_hifi/assembly.fasta|tr "\n" "@"|sed "s/@>/\n>/g"|sed "s/@/\n/;s/@//g"|head -2 >ref_ecoli_hifi.fasta

PATH_DATA=`pwd`

NAME="ecoli_hifi"
IN="$PATH_DATA/SRR12801740.fastq"
REF="$PATH_DATA/ref_ecoli_hifi.fasta"
echo -e "N: $NAME\nI: $IN\nR: $REF" >config_$NAME\.yaml
