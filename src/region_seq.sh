#!/bin/bash
if [ "$#" -lt 3 ]
then
    echo 'USAGE: ./seq_region.sh {input_start} {input_ref} {output} {region_size}'
else
    INPUT_START=$1
    INPUT_REF=$2
    OUTPUT=$3
    REGION_SIZE=$4
    START_REGION=`cat $INPUT_START`
    let "END_REGION = $REGION_SIZE + START_REGION -1"
    cat $INPUT_REF |grep ">" >$OUTPUT
    cat $INPUT_REF |grep -v ">"|tr -d "\n"|cut -c $START_REGION-$END_REGION >>$OUTPUT
fi
