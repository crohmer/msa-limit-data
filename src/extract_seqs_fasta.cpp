#include <fstream>
#include <string.h>
#include <iostream>
#include <list>

using namespace std;
string split_1(string chaine, char del ){
  string re ="";
  int size(chaine.size());
  int i(0);
  while(chaine[i] != del && i<size){
    re += chaine[i];
    i++;
  }
  return re;
}

int main(int argc, char** argv){
	string file_names_seq=argv[1];
	string file_fasta=argv[2];
	list <string> names_seq={};

	ifstream flux(file_names_seq.c_str());


	if(flux){
		string ligne("");
		while(getline(flux, ligne)){
			names_seq.push_back(ligne);
		}
	}
	else{
		cout << "ERROR: Unable to open the file." << endl;
		exit(0);
	}
	flux.close();

	flux.open(file_fasta.c_str());

	if(flux){
		string ligne("");
		bool found(false);
		while(getline(flux, ligne)){
			if (ligne[0] == '>'){
				string name_seq_fasta=split_1(ligne.substr(1, ligne.size()-1),' ');
				found=false;
				list <string>::iterator p=names_seq.begin();
				list <string>::iterator end=names_seq.end();
				while( !found && p!=end ){
					if(name_seq_fasta == *p){
						found=true;
					}
					else{
						p++;
					}
				}
				if (found) {
					names_seq.erase(p);
					std::cout << ligne << '\n';
				}
			}
			else{
				if (found) {
					std::cout << ligne << '\n';
				}
			}
		}
	}
	else{
		cout << "ERROR: Unable to open the file." << endl;
		exit(0);
	}
	flux.close();

  return 0;
}
