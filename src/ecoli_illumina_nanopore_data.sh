#!/bin/bash
THREAD="12"

if [ ! -d "ecoli_illumina_nanopore_data" ];then
mkdir "ecoli_illumina_nanopore_data"
fi

cd ecoli_illumina_nanopore_data

echo "Download nanopore reads..."
#fastq-dump --gzip SRR8335315 >/dev/null #Nanopore
#gunzip SRR8335315.fastq.gz >/dev/null
echo "Download illumina reads..."
#fastq-dump --gzip --split-spot --clip SRR8333590 >/dev/null #illumina
#gunzip SRR8333590.fastq.gz >/dev/null
echo "Download ecoli reference"
#wget  https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/004/358/385/GCF_004358385.1_ASM435838v1/GCF_004358385.1_ASM435838v1_genomic.fna.gz >/dev/null
#gunzip GCF_004358385.1_ASM435838v1_genomic.fna.gz >/dev/null

echo "Assembly of nanopore reads (flye)"
#flye --nano-raw  SRR8335315.fastq  --out ass_flye --genome-size 5500000 --threads $THREAD >/dev/null

echo "Assembly of illumina reads (spades)"
#spades.py -s SRR8333590.fastq -o spades_output_illumina --only-assembler -t $THREAD >/dev/null
echo "Assembly of illumina and nanopore reads (spades)"
#spades.py -s SRR8333590.fastq --nanopore SRR8335315.fastq -o spades_output_hybride --only-assembler -t $THREAD >/dev/null

#head -67952 GCF_004358385.1_ASM435838v1_genomic.fna >ref_ecoli.fasta
echo "Alignment of the nanopore contigs on the reference (minimap2)"
#minimap2 -cax asm5  -t $THREAD ref_ecoli.fasta assembly.fasta >aln_contigs_nanopore_ref_ecoli.sam
echo "Alignment of the illumina contigs on the reference (minimap2)"
#minimap2 -cax asm5  -t $THREAD ref_ecoli.fasta spades_output_illumina/contigs.fasta >aln_contigs_illumina_ref_ecoli.sam
echo "Alignment of the hybride contigs on the reference (minimap2)"
#minimap2 -cax asm5  -t $THREAD ref_ecoli.fasta spades_output_hybride/contigs.fasta >aln_contigs_hybride_ref_ecoli.sam
DEB=4845200;END=5080000
echo "Selection of the part of the nanopore contig that matches on a region of the reference."
#../src/read_map_region_v2.pl -s $DEB -e $END aln_contigs_nanopore_ref_ecoli.sam ref_nanopore.fasta
#echo "Selection of the part of the illumina contig that matches on a region of the reference."
#../src/read_map_region_v2.pl -s $DEB -e $END aln_contigs_illumina_ref_ecoli.sam ref_illumina.fasta
#echo "Selection of the part of the hybride contig that matches on a region of the reference."
#../src/read_map_region_v2.pl -s $DEB -e $END aln_contigs_hybride_ref_ecoli.sam ref_hybride.fasta

PATH_DATA=`pwd`

NAME_I="e_coli_ref_illumina"
NAME_N="e_coli_ref_nanopore"
IN="$PATH_DATA/SRR8335315.fastq"
REF_I="$PATH_DATA/ref_illumina.fasta"
REF_N="$PATH_DATA/ref_nanopore.fasta"
echo -e "N: $NAME_I\nI: $IN\nR: $REF_I" >config_$NAME_I.yaml
echo -e "N: $NAME_N\nI: $IN\nR: $REF_N" >config_$NAME_N.yaml
