#!/usr/bin/env python3
import sys,re,os
from Bio import AlignIO
from Bio.Align import AlignInfo
from Bio import pairwise2
from Bio.pairwise2 import format_alignment
from Bio import SeqIO

nuc_to_iupac={}
nuc_to_iupac["-"]="-"
nuc_to_iupac["A"]="A"
nuc_to_iupac["G"]="G"
nuc_to_iupac["C"]="C"
nuc_to_iupac["T"]="T"
nuc_to_iupac["AG"]="R"
nuc_to_iupac["CT"]="Y"
nuc_to_iupac["CG"]="S"
nuc_to_iupac["AT"]="W"
nuc_to_iupac["GT"]="K"
nuc_to_iupac["AC"]="M"
nuc_to_iupac["CGT"]="B"
nuc_to_iupac["AGT"]="D"
nuc_to_iupac["ACT"]="H"
nuc_to_iupac["ACG"]="V"
nuc_to_iupac["ACGT"]="N"

iupac_to_nuc={}
for key in nuc_to_iupac.keys():
	iupac_to_nuc[nuc_to_iupac[key]]=key

#Creation of the consensus sequence
def consensus_iupac(alignment,threshold):
	summary_align = AlignInfo.SummaryInfo(alignment)
	nb_reads = len(summary_align.get_column(0))
	align_size=len(alignment[0].seq)
	letters="ACGT-"
	seq=""
	for i in range(align_size):
		pass
		order_scores=[]
		order_letters=[]
		nucleotides_IUPAC=summary_align.get_column(i)
		nb_gap=len(re.findall("-",nucleotides_IUPAC))
		nucleotides_no_IUPAC=[]
		for nuc in nucleotides_IUPAC:
			nucleotides_no_IUPAC.append(iupac_to_nuc[nuc])
		order_scores=[]
		order_letters=[]
		for letter in letters:
			letter_score=0;
			for nuc in nucleotides_no_IUPAC:
				if(nuc.find(letter) > -1):
					letter_score+=1/len(nuc)
			if letter_score > 0:
				pass
				if order_scores == []:
					order_scores.append(letter_score)
					order_letters.append(letter)
				else:
					len_order_scores=len(order_scores)
					i=0
					find=0
					while (i<len_order_scores and find==0):
						if(letter_score>order_scores[i]):
							order_scores.append(order_scores[len_order_scores-1])
							order_letters.append(order_letters[len_order_scores-1])
							len_order_scores+=1
							j=len_order_scores-2
							while (j>i):
								order_scores[j]=order_scores[j-1]
								order_letters[j]=order_letters[j-1]
								j-=1
							order_scores[i]=letter_score
							order_letters[i]=letter
							find=1
						i+=1
					if i>=len_order_scores:
						order_scores.append(letter_score)
						order_letters.append(letter)
		#Gap closure
		if ("-" != order_letters[0]):
			if "-" in order_letters:
				i=order_letters.index("-")
				del order_letters[i]
				del order_scores[i]
			len_order_scores=len(order_scores)
			conserved_nucs=[]
			last_conserved_score=0
			total_frequency=0
			i=0
			while(i<len_order_scores-1 and (total_frequency < threshold*100 or order_scores[i]==last_conserved_score)):
				total_frequency+=100*order_scores[i]/(nb_reads - nb_gap)
				conserved_nucs=conserved_nucs + [order_letters[i]]
				last_conserved_score=order_scores[i]
				i+=1
			if(i==len_order_scores-1):
				if(total_frequency < threshold*100 or order_scores[i]==last_conserved_score):
					total_frequency+=100*order_scores[i]/(nb_reads - nb_gap)
					conserved_nucs=conserved_nucs + [order_letters[i]]
					last_conserved_score=order_scores[i]
			conserved_nucs.sort()
			if (len(conserved_nucs) > 1):
				id_iupac = "".join(conserved_nucs)
				seq+=nuc_to_iupac[id_iupac]
			else:
				seq += conserved_nucs[0]
		else:
			seq+="-"

	return seq

def majority_consensus(alignment):
	threshold=0.5
	summary_align = AlignInfo.SummaryInfo(alignment)
	nb_reads = len(summary_align.get_column(0))
	align_size = len(alignment[0].seq)
	seq=""

	#Run the alignment column by column
	for i in range(align_size):
		pass
		nucleotides_IUPAC=summary_align.get_column(i)
		freq_gap=len(re.findall("-",nucleotides_IUPAC))/nb_reads
		if(freq_gap < 0.5):
			frequency_letters={}
			nucleotides_no_IUPAC=[]
			for nuc in nucleotides_IUPAC:
				nucleotides_no_IUPAC.append(iupac_to_nuc[nuc])
			max_counter = 0
			max_letters=[]
			for letter in "ATGC":
				letter_counter=0;
				for nuc in nucleotides_no_IUPAC:
					if(nuc.find(letter) > -1):
						letter_counter+=1/len(nuc)

				if(letter_counter > max_counter):
					max_counter = letter_counter
					max_letters = [letter]
				else:
					if(letter_counter == max_counter):
						max_letters.append(letter)
			if ( len(max_letters) > 1):
				max_letters.sort()
				id_iupac = "".join((max_letters))
				seq+=nuc_to_iupac[id_iupac]
			else:
				seq+=max_letters[0]
		seq+="-"
	return seq

#Creation of the consensus sequence
def consensus(alignment,threshold):
	summary_align = AlignInfo.SummaryInfo(alignment)
	nb_reads = len(summary_align.get_column(0))
	align_size = len(alignment[0].seq)
	letters = summary_align._get_all_letters()
	letters = letters.replace('-','')
	seq=""

	#Run the alignment column by column
	for i in range(align_size):
		pass
		nucleotides=summary_align.get_column(i)
		frequency_letters={}
		max = [0,[]]

		#Gap closure
		frequency_gap = len(re.findall("-",nucleotides))/nb_reads
		if (frequency_gap < 0.50) :
			frequency_sum = 1 - frequency_gap
			#Calculates the frequency of each letter
			#and keeps the letter(s) with the maximum
			max_frequency = 0
			max_letters=[]
			for letter in letters:
				pass
				frequency_letter=(len(re.findall(letter,nucleotides))/nb_reads)/frequency_sum
				if max_frequency < frequency_letter:
					pass
					max_frequency = frequency_letter
					max_letters = [letter]
				else:
					if max_frequency==frequency_letter:
						pass
						max_letters = max_letters + [letter]

			#If the frequency < threshold, the consensus is undetermined
			#or if there is a tie
			if max_frequency < threshold or len(max_letters) > 1:
				pass
				seq+='N'
			#Not tie
			else:
				seq+=max_letters[0]
		seq+="-"
	return seq

#Index jaccard for two iupac
def index_jaccard(nuc_ref,nuc_cons):
	nb_nuc_ref = len(nuc_ref)
	nb_nuc_cons = len(nuc_cons)
	intersection = 0
	if(nb_nuc_ref <= nb_nuc_cons):
		i = 0
		while (i < nb_nuc_ref) :
			j = 0
			while(j < nb_nuc_cons and nuc_ref[i] != nuc_cons[j] ):
				j = j + 1
			if (j < nb_nuc_cons):
				intersection = intersection + 1
			i = i + 1
	else:
		i = 0
		while (i < nb_nuc_cons) :
			j = 0
			while(j < nb_nuc_ref and nuc_cons[i] != nuc_ref[j] ):
				j = j + 1
			if (j < nb_nuc_ref):
				intersection = intersection + 1
			i = i + 1
	return intersection / (nb_nuc_ref + nb_nuc_cons - intersection)

def score_between_two_iupac(seq_ref,seq_cons):
	score = 0
	size = len(seq_ref)
	for i in range(size):
		if (seq_ref[i] in iupac_to_nuc):
			nuc_ref = iupac_to_nuc[seq_ref[i]]
		else:
			nuc_ref = seq_ref[i]

		if (seq_cons[i] in iupac_to_nuc):
			nuc_cons = iupac_to_nuc[seq_cons[i]]
		else:
			nuc_cons = seq_cons[i]

		score = score + index_jaccard(nuc_ref,nuc_cons)
	return score
