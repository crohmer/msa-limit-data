#!/bin/bash
THREAD="24"

if [ ! -d "levure_data" ];then
    mkdir "levure_data"
fi

cd levure_data

PATH_DATA=`pwd`

echo "Creating configuration files..."
NAME="levure_bmb"
IN="$PATH_DATA/reads.fastq"
REF="$PATH_DATA/ref_chrX_W.fasta"
IN="/data/bonsai/crohmer/data/ERR4352154.fastq"
REF=" /data/bonsai/crohmer/data/ref_bmb.fasta"
echo -e "N: $NAME\nI: $IN\nR: $REF" >config_$NAME.yaml
NAME="levure_ccn"
IN="$PATH_DATA/reads.fastq"
REF="$PATH_DATA/ref_chrX_W.fasta"
IN="/data/bonsai/crohmer/data/ERR4352155.fastq"
REF="/data/bonsai/crohmer/data/ref_ccn.fasta"
echo -e "N: $NAME\nI: $IN\nR: $REF" >config_$NAME.yaml
NAME="levure_diploide"
IN="$PATH_DATA/reads.fastq"
REF="$PATH_DATA/ref_chrX_W.fasta"
IN="/data/bonsai/crohmer/data/read_diploide_shuffle.fastq"
REF="/data/bonsai/crohmer/data/seq_consensus.fasta"
echo -e "N: $NAME\nI: $IN\nR: $REF" >config_$NAME.yaml
