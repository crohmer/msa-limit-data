#!/usr/bin/perl -w
# use strict;
# use warnings;

#------------------------------------------------------------------------------
# VERIFICATION D'USAGE
#------------------------------------------------------------------------------
sub usage{
	print "\nshuffle_fastq.pl returns a fastq mix.\n".
		  "\nUSAGE: ./shuffle_fastq.pl <input.fastq>\n".
	      "\n";
	exit;
}

if (!exists $ARGV[0] || $ARGV[0] =~ '-h'){
	usage();
}

$FILE_INPUT = pop(@ARGV);
@TAB_FILE_INPUT = split(/\./,$FILE_INPUT);
$FORMAT = pop(@TAB_FILE_INPUT);
if ( !open(IN, "<", $FILE_INPUT) || ($FORMAT ne 'fastq') ){
	print "The file could not be opened or the format is incorrect.\n";
	usage();
}
#------------------------------------------------------------------------------
# DEBUT DU SCRIPT
#------------------------------------------------------------------------------

srand(123456);
#Lecture du fichier fastq
my $i=0;
my $nb_reads=-1;
my @tab=[];
while (<IN>)
{
  if ($i%4 == 0) {
    $nb_reads++;
    $tab[$nb_reads]=$_;
  }
  else{$tab[$nb_reads].=$_}

  $i++;
}
close IN;

my @urne=(0..($nb_reads));
my @tirage=();

for (1..($nb_reads+1))
{
	my $cardinal= @urne ;
	push @tirage,splice (@urne,rand $cardinal,1);
}

for (my $i = 0; $i <= $nb_reads; $i++) {
  print("$tab[$tirage[$i]]");
}
