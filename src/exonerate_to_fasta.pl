#!/usr/bin/perl -w
my $nb_align=0;
my $bool_first=1;
my $seq_1="";
my $seq_2="";
my $name_query="";
my $name_target="";
while (<>){
  if (/^C4 Alignment/){
    #print("$nb_align");
    if($nb_align==1){
      print(">$name_query\n");
      print("$seq_1\n");
      print(">$name_target\n");
      print("$seq_2\n");
    }
    $bool_first=1;
    $seq_1="";
    $seq_2="";
    $nb_align++;
  }
  if (/Query:/){
    chomp;
    ($before,$name_query) = split(/: /,$_);
  }
  if (/Target:/){
    chomp;
    ($before,$name_target) = split(/: /,$_);
  }
  if (/^ +[0-9]+ : /){
    ($before,$seq) = split(/ : /,$_);
    if ($bool_first){
      $seq_1.=$seq;
      $bool_first=0;
    }else{
      $seq_2.=$seq;
      $bool_first=1;
    }
  }
}
if($nb_align==1){
  print(">$name_query\n");
  print("$seq_1\n");
  print(">$name_target\n");
  print("$seq_2\n");
}
