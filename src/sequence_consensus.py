#!/usr/bin/env python3
import sys,re,os
from Bio import AlignIO
from Bio.Align import AlignInfo
from Bio import pairwise2
from Bio.pairwise2 import format_alignment
from Bio import SeqIO
import tool_consensus

"""
		Project "The Limits of Multiple Aligners"

Authors: ROHMER Coralie
Date: 2019-12

Description:
	Create a consensus sequence based on multiple alignment

"""
#Script use
def use():
	print("\nScript:\tsequence_consensus.py",
          "Objectif: Create a consensus sequence\n",
          "To Run:",
		  "./sequence_consensus.py [opt] -in <files> -t <threshold> -o <output_file>\n",
          "Arguments: ",
		  "  -required: -in : <string> <string> multiple alignment output file(s) (fasta/clustal).",
          "             -t  : <int> threshold above which the nucleotide is kept in percent.",
		  "				-o  : <string> output file",
          "  -optional: -h: display help.",
		  "             -I: use IUPAC code. (addition of nucleotide frequency until the threshold is passed)",
		  "             -m: use majority consensus. (useless -t)",
		  "             -g: <string> output file to obtain the consensus sequences with gaps\n",sep="\n")
	sys.exit()

#Arguments testing
try:
	help=sys.argv[sys.argv.index("-h")]
except:
	pass
else:
	use()

try:
    file_names=[sys.argv[sys.argv.index("-in")+1]]
except:
	print("ERROR: The name of the input file(s) is missing.\n")
	use()
else:
	i=2
	try:
		next_file = sys.argv[sys.argv.index("-in")+i]
	except:
		end_files = 1
	else:
		end_files = 0

	while ( end_files == 0 and re.search("-", next_file) == None ):
		file_names.append(next_file)
		i+=1
		try:
			next_file = sys.argv[sys.argv.index("-in")+i]
		except:
			end_files = 1
try:
	if (sys.argv[sys.argv.index("-m")]):
		majority=1
except:
	majority=0
try:
    threshold=float(sys.argv[sys.argv.index("-t")+1])/100
except:
	if (majority==False):
		print("ERROR: The nucleotide selection threshold is mandatory.\n")
		use()

try:
    output_file=sys.argv[sys.argv.index("-o")+1]
except:
	print("ERROR: Output file is missing.\n")
	use()

try:
	output_gap=sys.argv[sys.argv.index("-g")+1]
	gap=1
except:
	gap=0

try:
	pass
except Exception as e:
	raise

try:
	if (sys.argv[sys.argv.index("-I")]):
		iupac=1
except:
	iupac=0

out = open(output_file, "w")

if (gap):
	out_gap = open(output_gap, "w")
#Main
for file_name in file_names:
	pass

	#Open input file
	try:
		infile=open(file_name,"r")
	except:
		print("ERROR:The file",file_name,"does not exist.\n")
		use()

	#Storing the file in an alignment object (see Biopython)
	bool_file_type=re.search(".fasta", file_name)
	if bool_file_type != None:
	    pass
	    bool_fasta=re.search(">", infile.read())
		#alignments[0]=""
	    if bool_fasta != None:
	        pass
	        alignments = list(AlignIO.parse(file_name, "fasta"))
	    else:
	        alignments = list(AlignIO.parse(file_name, "clustal"))
	else:
		print("ERROR:The format of",file_name,"isn't correct.\n")
		use()
	#Retrieves the consensus sequence
	if (len(alignments) >= 1 ):
		if (iupac):
			seq=tool_consensus.consensus_iupac(alignments[0],threshold)
		else:
			if(majority):
				seq=tool_consensus.majority_consensus(alignments[0])
			else:
				seq=tool_consensus.consensus(alignments[0],threshold)
		if (gap):
			out_gap.write(">" + file_name.split("/")[-1].split(".")[0] + "\n" + seq + "\n")
		seq=seq.replace("-","")
		out.write(">" + file_name.split("/")[-1].split(".")[0] + "\n" + seq + "\n")
	else:
		print("",end="")

out.close()
if (gap):
	out_gap.close()
