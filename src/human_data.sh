#!/bin/bash
THREAD="24"

cd src/
g++ -Wall -o extract_fasta extract_seqs_fasta.cpp
cd ..

if [ ! -d "human_data" ];then
    mkdir "human_data"
fi

cd human_data

echo "Download nanopore reads..."
#wget https://s3-us-west-2.amazonaws.com/human-pangenomics/T2T/CHM13/nanopore/rel8-guppy-5.0.7/reads.fastq.gz
#gunzip reads.fastq.gz

echo "Download human reference"
#wget https://s3-us-west-2.amazonaws.com/human-pangenomics/T2T/CHM13/assemblies/chm13.draft_v1.1.fasta.gz
#gunzip chm13.draft_v1.1.fasta.gz

echo "Extract chrX"
tail -2571272 chm13.draft_v1.1.fasta|head -2570994 >ref_chrX_W.fasta

minimap2 -cax map-ont  -t $THREAD chm13.draft_v1.1.fasta reads.fastq >aln_nanopore_ref_chm13.sam
PATH_DATA=`pwd`

echo "Creating configuration files..."
NAME="human_ref_W"
IN="$PATH_DATA/reads.fastq"
REF="$PATH_DATA/ref_chrX_W.fasta"
echo -e "N: $NAME\nI: $IN\nR: $REF" >config_$NAME.yaml
