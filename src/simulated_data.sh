#!/bin/bash
if [ ! -d "simulated_data" ];then
mkdir "simulated_data"
fi

cd simulated_data

echo "Download reference..."
#wget https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/005/845/GCF_000005845.2_ASM584v2/GCF_000005845.2_ASM584v2_genomic.fna.gz >/dev/null
#gunzip GCF_000005845.2_ASM584v2_genomic.fna.gz >/dev/null
echo "Download model..."
#wget https://github.com/yukiteruono/pbsim2/raw/master/data/R103.model >/dev/null
REF="GCF_000005845.2_ASM584v2_genomic.fna"
SIZE=10000
DEPTH=400
MODEL=R103.model

for ERROR in 1 2 5 10 15 20 25 30;
do
  ACCURACY=$((100-$ERROR))

  #Deletion
  echo "Simulation of the simulated_del_$ERROR dataset (pbsim2)"
  #pbsim $REF --hmm_model $MODEL --depth $DEPTH --difference-ratio 00:00:100 --accuracy-mean 0.$ACCURACY --prefix simulated_del_$ERROR >/dev/null

  #Insertion
  echo "Simulation of the simulated_ins_$ERROR dataset (pbsim2)"
  #pbsim $REF --hmm_model $MODEL --depth $DEPTH --difference-ratio 00:100:00 --accuracy-mean 0.$ACCURACY --prefix simulated_ins_$ERROR >/dev/null

  #Substitution
  echo "Simulation of the simulated_sub_$ERROR dataset (pbsim2)"
  #pbsim $REF --hmm_model $MODEL --depth $DEPTH --difference-ratio 100:00:00 --accuracy-mean 0.$ACCURACY --prefix simulated_sub_$ERROR >/dev/null

  #Mixte
  echo "Simulation of the simulated_mixed_$ERROR dataset (pbsim2)"
  #pbsim $REF --hmm_model $MODEL --depth $DEPTH --difference-ratio 23:31:46 --accuracy-mean 0.$ACCURACY --prefix simulated_mixed_$ERROR >/dev/null
done

#rm *.maf *.ref
PATH_DATA=`pwd`

for SIMULATED in `ls |grep fastq`;
do NAME_SIMULATED=`echo $SIMULATED|sed "s/_0001.fastq//"`;
   echo -e "N: $NAME_SIMULATED\nI: $PATH_DATA/$SIMULATED\nR: $PATH_DATA/GCF_000005845.2_ASM584v2_genomic.fna" >config_$NAME_SIMULATED.yaml;
done
