#!/usr/bin/env python3
import subprocess,os

def usage():
    print("msa-limit-data: Data generation for msa-limit\n",
        "Usage:",
        'msa-limit-data.py',
        sep="\n")
    sys.exit()
def config_conda():
    try:
        open("src/config_conda.sh")
    except:
        result = subprocess.run("./src/create_config_conda.sh")
        try:
            open("src/config_conda.sh")
        except:
            print("Unable to launch the pipeline.")
            sys.exit()

config_conda()

if not os.path.exists("src/extract_reads"):
  result = subprocess.run(["g++", "src/extract_reads.cpp", "-o", "src/extract_reads"])

result = subprocess.run(["./src/snakemake_launcher.sh"])
